<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona_Medicamento extends Model
{
    use HasFactory;
    protected $fillable = ['observaciones','persona_id','medicamento_id'];
    public function personas(){
        return $this->belongsTo(Personas::class,'persona_id','id');
    }
    public function medicamentos(){
        return $this->belongsTo(Medicamentos::class,'medicamento_id','id');
    }
}
