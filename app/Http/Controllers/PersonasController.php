<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Personas;
use Illuminate\Validation\Rule;
class PersonasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $personas = Personas::all();
        return view('personas',['personas'=>$personas]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $persona = new Personas($request->input()); //toma todos los inputs para guardarlos

        $request->validate([
            'dni'=>'unique:personas,dni'
        ],[
            'dni.unique'=>'No pueden haber Personas iguales, DNI duplicado'
        ]);
        
        $persona->saveOrFail();
        return redirect()->route('personas.index');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $persona = Personas::find($id);
        return view('personas.editPerson',['persona'=>$persona]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        
        
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $persona = Personas::find($id);
        $request->validate([
            'dni' => [
                'required',
                'string',
                'max:255',
                Rule::unique('personas')->ignore($persona->id),
            ]
        ]);
        
        $persona->fill($request->input())->saveOrFail();
        return redirect()->route('personas.show',['persona'=>$persona]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $persona = Personas::find($id);
        $persona->delete();
        return redirect()->route('personas.index');
    }
}
