<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function personas(){
        return view('personas');
    }
    public function medicamentos(){
        return view('medicamentos.medicamentos');
    }
    public function persona_medicamento(){
        return view('persona_medicamento.persona_medicamento');
    }

}
