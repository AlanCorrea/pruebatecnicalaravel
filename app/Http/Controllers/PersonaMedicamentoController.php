<?php

namespace App\Http\Controllers;

use App\Models\Persona_Medicamento;
use Illuminate\Http\Request;
use App\Models\Personas;
use App\Models\Medicamentos;

class PersonaMedicamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $persona_medicamento = Persona_Medicamento::all();
        $personas = Personas::all();
        $medicamentos = Medicamentos::all();

        return view('persona_medicamento.persona_medicamento',
        ['persona_medicamento'=>$persona_medicamento,'personas'=>$personas,'medicamentos'=>$medicamentos]);
    }
    public function busquedaDni(Request $request){
        $persona = $request['busqueda'];
        $personas = Personas::all();
        $medicamentos = Medicamentos::all();
        //$consulta = Persona_medicamento::join('personas','persona_id','personas.id')->where('dni',$personas)->get();
        $consulta = Persona_medicamento::join('personas','persona_id','personas.id')->where('dni',$persona)->get();
        return view('persona_medicamento.persona_medicamento',
        ['persona_medicamento'=>$consulta,'personas'=>$personas,'medicamentos'=>$medicamentos]);
    }
    public function busquedaMedicamento(Request $request){
        $medicamento = $request['busqueda'];
        $personas = Personas::all();
        $medicamentos = Medicamentos::all();
        $consulta = Persona_medicamento::whereHas('medicamentos', function($query) use ($medicamento) {
            $query->where('nombre_comercial','LIKE','%'. $medicamento .'%');
        })->get();
        return view('persona_medicamento.persona_medicamento',
        ['persona_medicamento'=>$consulta,'personas'=>$personas,'medicamentos'=>$medicamentos]);
        
        
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $prescripciones = new Persona_Medicamento($request->only(['persona_id','medicamento_id','observaciones']));
        $prescripciones->saveOrFail();
        return redirect()->route('persona_medicamento.index');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $persona_medicamento = Persona_Medicamento::find($id);
        $personas = Personas::all();
        $medicamentos = Medicamentos::all();
        return view('persona_medicamento.editPrescripcion',
        ['persona_medicamento'=>$persona_medicamento,'personas'=>$personas,'medicamentos'=>$medicamentos]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $persona_medicamento = Persona_Medicamento::find($id);
        $persona_medicamento->fill($request->only(['persona_id','medicamento_id','observaciones']))->saveOrFail();
        return redirect()->route('persona_medicamento.show', ['persona_medicamento'=>$persona_medicamento]);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $persona_medicamento = Persona_Medicamento::find($id);
        $persona_medicamento->delete();
        return redirect()->route('persona_medicamento.index');
    }
    
}
