<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Medicamentos;
use Illuminate\Validation\Rule;

class MedicamentosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $medicamentos = Medicamentos::all();
        return view('medicamentos.medicamentos',['medicamentos'=>$medicamentos]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $medicamentos = new Medicamentos($request->input()); //toma todos los inputs para guardarlos

        $request->validate([
            'nombre_comercial'=>'unique:medicamentos,nombre_comercial'
        ],[
            'nombre_comercial.unique'=>'No pueden haber medicamentos iguales, campo duplicado'
        ]);
        
        $medicamentos->saveOrFail();
        return redirect()->route('medicamentos.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $medicamento = Medicamentos::find($id);
        return view('medicamentos.editMed',['medicamento'=>$medicamento]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $medicamento = Medicamentos::find($id);
        $request->validate([
            'nombre_comercial' => [
                'required',
                'string',
                'max:255',
                Rule::unique('medicamentos')->ignore($medicamento->id),
            ]
        ]);
        $medicamento->fill($request->input())->saveOrFail();
        return redirect()->route('medicamentos.show',['medicamento'=>$medicamento]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $medicamento = Medicamentos::find($id);
        $medicamento->delete();
        return redirect()->route('medicamentos.index');
    }
}
