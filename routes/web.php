<?php

use App\Http\Controllers\PageController;
use App\Http\Controllers\PersonasController;
use App\Http\Controllers\MedicamentosController;
use App\Http\Controllers\PersonaMedicamentoController;
use App\Models\Persona_Medicamento;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PersonasController::class,'index']);
//Route::get('medicamentos',[PageController::class,'medicamentos'])->name('medicamentos');
Route::resource('personas',PersonasController::class);
Route::resource('medicamentos',MedicamentosController::class);
Route::resource('persona_medicamento',PersonaMedicamentoController::class);
Route::get('persona_medicamneto/busquedaDni',[PersonaMedicamentoController::class, 'busquedaDni'])->name('busquedaDni');
Route::get('persona_medicamneto/busquedaMedicamento',[PersonaMedicamentoController::class, 'busquedaMedicamento'])->name('busquedaMedicamento');
