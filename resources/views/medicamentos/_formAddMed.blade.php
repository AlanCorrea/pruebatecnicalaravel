
<div class="modal fade modal-lg" id="agregar_medicamento" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="agregar_persona" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="agregar_medicamento">Agregar Medicamento</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('medicamentos.store')}}" method="POST">
            @csrf
            <div class="row text-center justify-content-center">
                <div class="col-6">
                    <label class="form-label" for="nombre_comercial">Nombre comercial</label>
                    <input name="nombre_comercial" type="text" class="form-control" placeholder="Nombre comercial" required>
                </div>
            </div>
                <button type="button btn btn-lg" class="btn btn-primary m-3">Agregar</button>
        </form>
      </div>
      
    </div>
  </div>
</div>