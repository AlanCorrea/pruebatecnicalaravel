@extends('template.templateHead')
@section('body')
<div class="container">
    <div class="row">
        <div class="col-12">
           
            <div class="card m-5">
                <div class="card-body">
                    <div class="card-title h1 text-center m-4">Editar medicamento</div>
                    @error('nombre_comercial')
                        <div class="alert alert-danger" role="alert">
                            {{$message}} 
                        </div>
                    @enderror
                    <form action="{{route('medicamentos.update',[$medicamento])}}" method="POST">
                        @method('PATCH')
                        @csrf
                        @csrf
                        <div class="row text-center justify-content-center">
                            <div class="col-6">
                                <label class="form-label" for="nombre_comercial">Nombre comercial</label>
                                <input name="nombre_comercial" type="text" class="form-control" placeholder="Nombre comercial" value="{{$medicamento->nombre_comercial}}" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 justify-content-center text-center align-items-center m-2">
                                <input type="submit" class="btn btn-primary btn-lg" value="Modificar">
                            </div>
                    </form>
                    <div class="row justify-content-center text-center align-items-center m-2">
                            <div class="col-4 ">
                                <a href="{{route('medicamentos.index')}}"><button type="button" class="btn btn-primary btn-xs">Atrás</button></a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection