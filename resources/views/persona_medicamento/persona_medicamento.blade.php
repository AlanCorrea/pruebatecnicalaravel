@extends('../template/template')
@section('agregar')
<button class="btn btn-success btn-lg" id="btnPersona" data-bs-toggle="modal" data-bs-target="#agregar_prescripcion">Agregar Prescripción<i class="mx-1 fa-solid fa-circle-plus"></i></button>
@endsection
@section('buscador')
    <form action="{{route('busquedaDni')}}" method="GET">
        <div class="row">
            <div class="col-xl-6">
                <input type="text" name="busqueda" class="form-control" placeholder="filtrar por dni">
            </div>
            <div class="col-xl-1">
                <button class="button btn btn-success">Buscar</button>
            </div>
        </div>
    </form>
    <form action="{{route('busquedaMedicamento')}}" method="GET" class="mt-2">
        <div class="row">
            <div class="col-xl-6">
                <input type="text" name="busqueda" class="form-control" placeholder="filtrar medicamento">
            </div>
            <div class="col-xl-1">
                <button class="button btn btn-success">Buscar</button>
            </div>
        </div>
    </form>
@endsection
@section('tabla')
    <th>
        <tr class="table-dark">
            <td>id</td>
            <td>Nombre</td>
            <td>Apellido</td>
            <td>Dni</td>
            <td>Medicamento</td>
            <td>Observaciones</td>
            <td>Fecha de prescripcion</td>
            <td>Editar</td>
            <td>Eliminar</td>
        </tr>
    </th>

@include('persona_medicamento._formAddPrescripcion')
@endsection
@section('cuerpo_tabla')
    @foreach($persona_medicamento as $value)
        <tr>
            <td>{{$value->id}}</td>
            <td>{{$value->personas->nombre}}</td>
            <td>{{$value->personas->apellido}}</td>
            <td>{{$value->personas->dni}}</td>
            <td>{{$value->medicamentos->nombre_comercial}}</td>
            <td>{{$value->observaciones}}</td>
            <td>{{$value->created_at}}</td>
            <td>
                <a href="{{ route('persona_medicamento.show',[$value])}}"><button class="mx-2 btn btn-info border-dark"><i class="fa-regular fa-pen-to-square"></i></button></a>
            </td>
            <td>
                <form action="{{ route('persona_medicamento.destroy',[$value]) }}" method="POST" >
                    @method('delete')
                    @csrf
                    <button class="mx-2 btn btn-danger border-dark"><i class="fa-sharp fa-solid fa-trash"></i></button>
                </form>
            </td>
        </tr>
    @endforeach
    
@endsection
