
<div class="modal fade modal-lg" id="agregar_prescripcion" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="agregar_persona" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="agregar_prescripcion">Agregar prescripcion</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('persona_medicamento.store')}}" method="POST">
            @csrf
            <div class="row text-center justify-content-center">
                <div class="col-6">
                    <label class="form-label" for="persona_id">Persona</label>
                    <select class="form-select" name="persona_id">
                        @foreach($personas as $value)
                            <option value="{{$value->id}}">{{$value->nombre}} {{$value->apellido}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row text-center justify-content-center">
                <div class="col-6 m-2">
                    <label class="form-label" for="medicamento_id">Medicamento</label>
                    <select class="form-select" name="medicamento_id">
                        @foreach($medicamentos as $medicamento)
                            <option value="{{$medicamento->id}}">{{$medicamento->nombre_comercial}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row text-center justify-content-center">
                <div class="col-6 m-2">
                    <label class="form-label" for="observaciones">Observaciones</label>
                    <input name="observaciones" type="text" class="form-control" placeholder="observaciones" required>
                </div>
            </div>
                <button type="button btn btn-lg" class="btn btn-primary m-3">Agregar</button>
        </form>
      </div>
      
    </div>
  </div>
</div>