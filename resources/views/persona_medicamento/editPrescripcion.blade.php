@extends('template.templateHead')
@section('body')
<div class="container">
    <div class="row">
        <div class="col-12">
           
            <div class="card m-5">
                <div class="card-body">
                    <div class="card-title h1 text-center m-4">Editar Prescripcion</div>
                    <form action="{{route('persona_medicamento.update',[$persona_medicamento])}}" method="POST">
                        @method('PATCH')
                        @csrf
                        <div class="row text-center justify-content-center">
                            <div class="col-6">
                                <label class="form-label" for="persona_id">Persona</label>
                                <select class="form-select" name="persona_id">
                                    @foreach($personas as $value)
                                        <option value="{{$value->id}}" 
                                        @if ($value->id == $persona_medicamento->persona_id) selected @endif
                                        >{{$value->nombre}} {{$value->apellido}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row text-center justify-content-center">
                            <div class="col-6 m-2">
                                <label class="form-label" for="medicamento_id">Medicamento</label>
                                <select class="form-select" name="medicamento_id">
                                    
                                    @foreach($medicamentos as $medicamento)
                                        <option value="{{$medicamento->id}}" 
                                        @if ($medicamento->id == $persona_medicamento->medicamento_id) selected @endif >
                                        {{$medicamento->nombre_comercial}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row text-center justify-content-center">
                            <div class="col-6 m-2">
                                <label class="form-label" for="observaciones">Observaciones</label>
                                <input name="observaciones" type="text" class="form-control" 
                                placeholder="observaciones" value="{{$persona_medicamento->observaciones}}" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 justify-content-center text-center align-items-center">
                                <input type="submit" class="btn btn-primary btn-lg" value="Modificar">
                            </div>
                        </div>
                    </form>
                    <div class="row justify-content-center text-center align-items-center m-2">
                            <div class="col-4 ">
                                <a href="{{route('persona_medicamento.index')}}"><button type="button" class="btn btn-primary btn-xs">Atrás</button></a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection