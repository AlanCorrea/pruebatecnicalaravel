<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @vite(['resources/css/app.scss','resources/js/app.js'])
    <title>Document</title>
</head>
<body>
    <div class="container mt-5">
        <div class="row">
            <h1>Crud Prescripciones</h1>
            <div id="contenedor" class=" col-12 text-center justify-content-center align-items-center">
                <div  class="card border-1 p-4 text-light" id="primaryCard" style="width:max-content;box-sizing:border-box">
                <nav>
                    <div class="card-title ">
                        <div class="btn-group col-12">
                                <a href="{{route('personas.index')}}" class="btn btn-primary btn-lg" aria-current="page" id="PersonasTable">Personas</a>
                                <a href="{{route('medicamentos.index')}}" class="btn btn-primary btn-lg" id="medicamentosTable">Medicamentos</a>
                                <a href="{{route('persona_medicamento.index')}}" class="btn btn-primary btn-lg" id="prescripcionesTable">Prescripciones médicas</a>
                                @yield('agregar')
                        </div>                        
                    </div>
                </nav>
                    <div class="card-body" id="bodyt" >
                        @yield('buscador')
                        @yield('error')
                        
                        <div class="m-10 col-12 text-dark text-center m-auto d-flex justify-content-center align-items-center my-3">
                        

                            <table class="table table-responsive table-light table-border table-hover text-center" id = "tabla">
                                @yield('tabla')
                                <tbody>
                                    @yield('cuerpo_tabla')
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>