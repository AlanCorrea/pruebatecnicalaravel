@extends('template.template')
@section('agregar')
<button class="btn btn-success btn-lg" data-bs-toggle="modal" data-bs-target="#agregar_persona" 
id="btnPersona">Agregar Persona<i class="mx-1 fa-solid fa-circle-plus"></i></button>
@endsection
@section('error')
    @error('dni')
        <div class="alert alert-danger" role="alert">
            {{$message}} 
        </div>
    @enderror
@endsection
@section('tabla')
    <th>
        <tr class="table-dark">
            <td>id</td>
            <td>Nombre</td>
            <td>Apellido</td>
            <td>Dni</td>
            <td>Fecha de nacimiento</td>
            <td>Editar</td>
            <td>Eliminar</td>

        </tr>
    </th>
@endsection
@section('cuerpo_tabla')
    @foreach($personas as $value)
        <tr>
            <td>{{$value->id}}</td>
            <td>{{$value->nombre}}</td>
            <td>{{$value->apellido}}</td>
            <td>{{$value->dni}}</td>
            <td>{{$value->fecha_nacimiento}}</td>
            <td>
                
                   <a href="{{ route('personas.show',[$value])}}"><button class="mx-2 btn btn-info border-dark"><i class="fa-regular fa-pen-to-square"></i></button></a>
            </td>
            <td>
            <form action="{{ route('personas.destroy',[$value]) }}" method="POST" >
                    @method('delete')
                    @csrf
                    <button class="mx-2 btn btn-danger border-dark"><i class="fa-sharp fa-solid fa-trash"></i></button>
                </form>
            </td>
        </tr>
    @endforeach
<!-- Modal de persona-->
@include('personas._formAdd')

@endsection