@extends('template.templateHead')
@section('body')
<div class="container">
    <div class="row">
        <div class="col-12">
           
            <div class="card m-5">
                <div class="card-body">
                    <div class="card-title h1 text-center m-4">Editar Persona</div>
                    @error('dni')
                <div class="alert alert-danger" role="alert">
                    {{$message}} 
                </div>
            @enderror
                    <form action="{{route('personas.update',[$persona])}}" method="POST">
                        @method('PATCH')
                        @csrf
                        <div class="row">
                            <div class="col-4">
                                <label class="form-label" for="nombre">Nombre</label>
                                <input name="nombre" type="text" class="form-control" placeholder="Nombre" value="{{$persona->nombre}}" required>
                            </div>
                            <div class="col-4">
                                <label class="form-label" for="apellido">Apellido</label>
                                <input name="apellido" type="text" class="form-control" placeholder="Apellido" value="{{$persona->apellido}}" required>
                            </div>
                            <div class="col-4">
                                <label class="form-label" for="dni">DNI</label>
                                <input name="dni" type="text" class="form-control" placeholder="Numero de DNI" value="{{$persona->dni}}" required>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-4">
                                <label class="form-label pt-3" for="fecha_nacimiento">Fecha de Nacimiento</label>
                                <input type="date" class="form-control" name="fecha_nacimiento" placeholder="fecha de nacimiento" value="{{$persona->fecha_nacimiento}}" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 justify-content-center text-center align-items-center">
                                <input type="submit" class="btn btn-primary btn-lg" value="Modificar">
                            </div>
                        </div>
                    </form>
                    <div class="row justify-content-center text-center align-items-center m-2">
                            <div class="col-4 ">
                                <a href="{{route('personas.index')}}"><button type="button" class="btn btn-primary btn-xs">Atrás</button></a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
        
@endsection
  
