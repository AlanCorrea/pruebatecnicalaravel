
<div class="modal fade modal-lg" id="agregar_persona" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="agregar_persona" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="agregar_persona">Agregar una persona</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('personas.store')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-4">
                    <label class="form-label" for="nombre">Nombre</label>
                    <input name="nombre" type="text" class="form-control" placeholder="Nombre" required>
                </div>
                <div class="col-4">
                    <label class="form-label" for="apellido">Apellido</label>
                    <input name="apellido" type="text" class="form-control" placeholder="Apellido" required>
                </div>
                <div class="col-4">
                    <label class="form-label" for="dni">DNI</label>
                    <input name="dni" type="text" class="form-control" placeholder="Numero de DNI" required>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <label class="form-label pt-3" for="fecha_nacimiento">Fecha de Nacimiento</label>
                    <input type="date" class="form-control" name="fecha_nacimiento" placeholder="fecha de nacimiento" required>
                </div>
            </div>
                <button type="button btn btn-lg" class="btn btn-primary">Agregar</button>
        </form>
      </div>
      
    </div>
  </div>
</div>